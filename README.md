## About
This is an extension/interfacing library for using the [Djula](https://mmontone.github.io/djula/) templating system with Radiance. Particularly, it adds the following functionality:

* An option called `:djula` for `radiance:page`, `admin:panel`, and `profile:panel`. It takes the name of a template in the modules `template` directory. If given, it will parse the document when the page is called, and store it in the `*template*` variable. The page's body should return the rendered page text by calling `render-template`. The content-type of the response is set to `"text/html"`.
* A template tag named `uri` which translates an internal URI to an external URL by calling `radiance:uri-to-url` and specifiying `:representatin external`.
* A template tag named `resource` which can be used to look up a resource by calling `radiance:resource` and translating to an external URL.
