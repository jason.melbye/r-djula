;;;; r-djula.asd

(asdf:defsystem #:r-djula
  :defsystem-depends-on (:radiance)
  :class "radiance:virtual-module"
  :description "Wrapper around djula to better integrate with radiance."
  :author "Jason Melbye <jason.melbye@gmail.com>"
  :version "1.0.0"
  :license "zlib"
  :serial t
  :components ((:file "package")
               (:file "r-djula"))
  :depends-on (:djula))

