;;;; r-djula.lisp

(in-package #:r-djula)

;;; "r-djula" goes here. Hacks and glory await!

(defvar *template*)

(defvar *compiled-templates* (make-hash-table))

(defun compiled-template (template-pathname)
  (check-type template-pathname pathname)
  (multiple-value-bind (compiled hit)
      (gethash template-pathname *compiled-templates*)
    (if hit
	compiled
	(setf (gethash template-pathname *compiled-templates*)
	      (djula:compile-template* (djula:find-template* template-pathname))))))

(defun render-template (template &rest template-arguments)
  (apply #'djula:render-template*
	 (etypecase template
	   ((eql t) *template*)
	   (pathname template)
	   (string template))
	 nil template-arguments))

(defun transform-body (body template)
  (if template
      `((let ((*template* (compiled-template (@template ,template))))
	  (setf (content-type *response*) "text/html")
	  ,@body))
      body))

(define-option radiance:page :djula (name body uri &optional template)
  (declare (ignore name uri))
  (transform-body body template))

(define-option admin:panel :djula (name body category &optional template)
  (declare (ignore name category))
  (transform-body body template))

(define-option profile:panel :djula (name body &optional template)
  (declare (ignore name))
  (transform-body body template))

(djula:def-tag-compiler :url (uri)
  (lambda (stream)
    (princ #\" stream)
    (princ (radiance:uri-to-url uri :representation :external) stream)
    (princ #\" stream)))

(djula:def-tag-compiler :resource (module type name &optional referer)
  (lambda (stream)
    (multiple-value-bind (base query fragment)
	(radiance:resource module type name referer)
      (princ #\" stream)
      (princ (uri-to-url base
			 :query query
			 :fragment fragment
			 :representation :external)
	     stream)
      (princ #\" stream))))
