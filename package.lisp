;;;; package.lisp

(in-package #:modularize-user)
(define-module #:r-djula
  (:use #:cl
	#:radiance)
  (:export #:render-template))

(in-package #:r-djula)

